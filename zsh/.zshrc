#!/bin/zsh

autoload -U colors && colors
#PS1=' %B%F{251}%1~  %b%F{245} '
PS1=' %B%F{251}*  %b%F{245} '

autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh

bindkey -v 

HISTSIZE=10000
SAVEHIST=10000
HISTFILE=~/.cache/zhistory

source ~/.config/zsh/aliasrc
